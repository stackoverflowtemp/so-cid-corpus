I'm starting out a new Android project that. The project is referencing one third-party JAR that in turn references 4 other jars:

ws-commons-java5-1.0.1.jar
ws-commons-util-1.0.2.jar
xmlrpc-client-3.1.3.jar
xmlrpc-common-3.1.3.jar


As far as I can tell, I think these 4 jars are standard Java classes I think. Anyways, I have all five of these jars (the 4 above + the 1 third party jar) in my Build Path. When I attempt to run my Android app, I get:

<error>Dx
trouble processing "javax/xml/XMLConstants.class":

Ill-advised or mistaken usage of a core class (java.* or javax.*)
when not building a core library.

[2012-03-21 23:05:05 - MyApp ] Dx 1 error; aborting
[2012-03-21 23:05:05 - MyApp ] Conversion to Dalvik format failed with error 1
</error>

I'm not sure what to do here. It's like it's asking me if I'm trying to build a <code>core-library</code>, which I'm not. I'm just trying to use these JARs in my project. Is there another way I should be referencing these JARs in my project? The error makes it sound like my project is going to randomly stop working for someone someday.

This is all very confusing. Any clues on how to properly proceed?


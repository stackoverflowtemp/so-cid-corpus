I am trying to develop application for SMS encryption. I have to communicate public key through SMS. Now i am being able to do encryption/decryption by generating the key pair(on same mobile client) but when i send the key as encoded string then on other side i am unable to recover the <code>public</code> key from string. Code is given below. In <code>strToPublicKey()</code> function, parameters are not returned in <code>obj</code>.

<javacode>byte[] keyBytes = Base64.decode(getPublicKey());
PublicKey publickey = strToPublicKey(new String(keyBytes));

//private String publicKey;
private String getPublicKey()
{
// this string is received through SMS.. here it is hardcoded for testing purpose
// Base 64 encoded public key
return "LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0NCk1JR2ZNQTBHQ1NxR1NJYjNEUUVCQVFVQUE0R05BRENCaVFLQmdRRE1PZGtaN28yMmJiSTAxZnY5QWtvMXFmTHd6MzRBYlA3ejgrcUMNCi9XZGNHdEFmdGk1QUFvZWUxOTV2NUd1QTZhak9JS3Y1cDkzdEdFNitWamdRdG03d0phS25kMno1UGpvZHIvc2R5bW9hNGdZT2dXcHQNCmVvamRybFZmeHZBbUxUN0JsWEExNGNuQUxDc3prYUtmTktSeEIrWDZlZGIyQkVaVnlKYThQcTBLYndJREFRQUINCi0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLQ==";
}

public static PublicKey strToPublicKey(String s)
{

PublicKey pbKey = null;

BufferedReader br   = new BufferedReader( new StringReader(s) );
PEMReader pr        = new PEMReader(br);
Object obj = pr.readObject();

if( obj instanceof PublicKey )
{
pbKey = (PublicKey) pr.readObject();
}
else if( obj instanceof KeyPair )
{
KeyPair kp = (KeyPair) pr.readObject();
pbKey = kp.getPublic();
}
pr.close();
return pbKey;
}
</javacode>

there are no errors in logcat

<error>03-01 06:54:00.054: I/System.out(4033): debugger has settled (1412)
03-01 06:54:01.614: D/dalvikvm(4033): GC_CONCURRENT freed 211K, 12% free 2573K/2904K, paused 75ms+76ms, total 251ms

03-01 06:54:02.784: D/dalvikvm(4033): GC_CONCURRENT freed 208K, 11% free 2763K/3096K, paused 77ms+5ms, total 176ms

03-01 06:54:06.384: D/dalvikvm(4033): GC_CONCURRENT freed 128K, 8% free 3025K/3280K, paused 75ms+94ms, total 282ms

03-01 06:54:09.866: D/dalvikvm(4033): GC_CONCURRENT freed 269K, 12% free 3144K/3540K, paused 76ms+93ms, total 259ms

03-01 06:56:52.745: D/dalvikvm(4033): threadid=1: still suspended after undo (sc=1 dc=1)
03-01 06:56:52.765: D/dalvikvm(4033): GC_CONCURRENT freed 235K, 10% free 3316K/3680K, paused 83ms+9ms, total 208ms
</error>
there certainly is some casting issue...please someone help me:(


Firstly, sorry for my level in english.

I need some help. I'm trying to develop an app (a <code>soundbox</code> you click on a button and you hear a sound). I had no problem but after having had some songs, I have a memory problem. Here is my log :

<error>enter code here11-10 15:21:02.446: E/Trace(942): error opening trace file: No such file         or directory (2)
11-10 15:21:03.535: D/dalvikvm(942): GC_CONCURRENT freed 83K, 2% free 8369K/8519K,         paused 83ms+4ms, total 149ms
11-10 15:21:03.535: D/dalvikvm(942): WAIT_FOR_CONCURRENT_GC blocked 24ms
11-10 15:21:03.795: D/dalvikvm(942): GC_CONCURRENT freed 10K, 2% free 8813K/8903K, paused    74ms+17ms, total 134ms
11-10 15:21:04.026: I/Choreographer(942): Skipped 46 frames!  The application may be    doing too much work on its main thread.
11-10 15:21:04.035: D/gralloc_goldfish(942): Emulator without GPU emulation detected.
11-10 15:21:04.276: D/AndroidRuntime(942): Shutting down VM
11-10 15:21:04.276: W/dalvikvm(942): threadid=1: thread exiting with uncaught exception  (group=0x40a13300)
11-10 15:21:04.315: E/AndroidRuntime(942): FATAL EXCEPTION: main
11-10 15:21:04.315: E/AndroidRuntime(942): java.lang.RuntimeException: Expecting menu,   got TabHost
11-10 15:21:04.315: E/AndroidRuntime(942):  at   android.view.MenuInflater.parseMenu(MenuInflater.java:143)
11-10 15:21:04.315: E/AndroidRuntime(942):  at  android.view.MenuInflater.inflate(MenuInflater.java:110)
11-10 15:21:04.315: E/AndroidRuntime(942):  at    com.example.famous.movies.sounds_.MainActivity.onCreateOptionsMenu(MainActivity.java:46)
11-10 15:21:04.315: E/AndroidRuntime(942):  at   android.app.Activity.onCreatePanelMenu(Activity.java:2476)
11-10 15:21:04.315: E/AndroidRuntime(942):  at   com.android.internal.policy.impl.PhoneWindow.preparePanel(PhoneWindow.java:393)
11-10 15:21:04.315: E/AndroidRuntime(942):  at   com.android.internal.policy.impl.PhoneWindow.invalidatePanelMenu(PhoneWindow.java:747)
11-10 15:21:04.315: E/AndroidRuntime(942):  at   com.android.internal.policy.impl.PhoneWindow$1.run(PhoneWindow.java:2913)
11-10 15:21:04.315: E/AndroidRuntime(942):  at   android.os.Handler.handleCallback(Handler.java:615)
11-10 15:21:04.315: E/AndroidRuntime(942):  at  android.os.Handler.dispatchMessage(Handler.java:92)
11-10 15:21:04.315: E/AndroidRuntime(942):  at android.os.Looper.loop(Looper.java:137)
11-10 15:21:04.315: E/AndroidRuntime(942):  at  android.app.ActivityThread.main(ActivityThread.java:4745)
11-10 15:21:04.315: E/AndroidRuntime(942):  at   java.lang.reflect.Method.invokeNative(Native Method)
11-10 15:21:04.315: E/AndroidRuntime(942):  at  java.lang.reflect.Method.invoke(Method.java:511)
11-10 15:21:04.315: E/AndroidRuntime(942):  at   com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:786)
11-10 15:21:04.315: E/AndroidRuntime(942):  at   com.android.internal.os.ZygoteInit.main(ZygoteInit.java:553)
11-10 15:21:04.315: E/AndroidRuntime(942):  at dalvik.system.NativeStart.main(Native Method)
</error>

How can I fix it ? I have put two tabs but the songs are just on the first one. Here is the code java :

<javacode>public class MainActivity extends TabActivity {

@Override
public void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.activity_main);

// Gestion des onglets

// Tout d'abord on récupère les ressources qui seront utilisés plus tard :
Resources res = getResources();
// On prépare les éléments nécessaires pour chaque onglet :
TabHost tabHost = getTabHost();
TabHost.TabSpec spec;
Intent intent;



// on lie nos onglets à la principale activity :
// ONGLET 1
intent = new Intent().setClass(this, Tab1.class);
spec = tabHost.newTabSpec("Widget").setIndicator("Cinema", res.getDrawable(R.drawable.movies)).setContent(intent);
tabHost.addTab(spec);

// ONGLET 2
intent = new Intent().setClass(this, Tab2.class);
spec = tabHost.newTabSpec("Form").setIndicator("Infos", res.getDrawable(R.drawable.infos)).setContent(intent);
tabHost.addTab(spec);

// On choisit l'onglet par défaut
tabHost.setCurrentTab(0);
}

@Override
public boolean onCreateOptionsMenu(Menu menu) {
getMenuInflater().inflate(R.layout.activity_main, menu);
return true;
}
}



public class Tab1 extends Activity {

private MediaPlayer mPlayer = null;


/** Called when the activity is first created. */
@Override
public void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.onglet1);

final int sound11 = R.raw.carjacker;
final int sound12 = R.raw.petitesoeur;
final int sound13 = R.raw.cracherlama;
final int sound14 = R.raw.maitresse;
final int sound21 = R.raw.gandalfen;
final int sound22 = R.raw.gandalf;
final int sound23 = R.raw.gollum;
final int sound24 = R.raw.gollumen;
final int sound31 = R.raw.pasdebraspasdechocolat;
final int sound32 = R.raw.sparta;
final int sound33 = R.raw.sabre;
final int sound34 = R.raw.darkvador;
final int sound41 = R.raw.back;
final int sound42 = R.raw.fightclub;
final int sound43 = R.raw.forrestgump;
final int sound44 = R.raw.iamyourfather;
final int sound51 = R.raw.loki;
final int sound52 = R.raw.dinercon1;
final int sound53 = R.raw.gollum;
final int sound54 = R.raw.gollum;
/*  final int sound61 = R.raw.dinercondroit4;
final int sound62 = R.raw.dinerconfemme5;
final int sound63 = R.raw.dinerconchampion6;
final int sound64 = R.raw.dinerconsonne7;
*/
generateSoundOnButton(sound11, R.id.btn11);
generateSoundOnButton(sound12, R.id.btn12);
generateSoundOnButton(sound13, R.id.btn13);
generateSoundOnButton(sound14, R.id.btn14);
generateSoundOnButton(sound21, R.id.btn21);
generateSoundOnButton(sound22, R.id.btn22);
generateSoundOnButton(sound23, R.id.btn23);
generateSoundOnButton(sound24, R.id.btn24);
generateSoundOnButton(sound31, R.id.btn31);
generateSoundOnButton(sound32, R.id.btn32);
generateSoundOnButton(sound33, R.id.btn33);
generateSoundOnButton(sound34, R.id.btn34);
generateSoundOnButton(sound41, R.id.btn41);
generateSoundOnButton(sound42, R.id.btn42);
generateSoundOnButton(sound43, R.id.btn43);
generateSoundOnButton(sound44, R.id.btn44);
generateSoundOnButton(sound51, R.id.btn51);
generateSoundOnButton(sound52, R.id.btn52);
generateSoundOnButton(sound53, R.id.btn53);
generateSoundOnButton(sound54, R.id.btn54);
/*
generateSoundOnButton(sound61, R.id.btn61);
generateSoundOnButton(sound62, R.id.btn62);
generateSoundOnButton(sound63, R.id.btn63);
generateSoundOnButton(sound64, R.id.btn64);
*/
}


public void generateSoundOnButton(final int soundId, int idButton) {

final ImageButton btn = (ImageButton) findViewById(idButton);

btn.setOnClickListener(new OnClickListener() {

public void onClick(View v) {
playSound(soundId);
}

});
}


@Override
public void onPause() {
super.onPause();
if(mPlayer != null) {
mPlayer.stop();
mPlayer.release();
}
}

private void playSound(int resId) {

if(mPlayer != null) {
mPlayer.stop();
mPlayer.release();
}
mPlayer = MediaPlayer.create(this, resId);
mPlayer.start();
}

}



public class Tab2 extends Activity {


/** Called when the activity is first created. */
@Override
public void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.onglet2);
}
}
</javacode>

Actually at the beginning I removed the methode <javacode>mPlayer.stop()</javacode> because there were bugs and without this method it was working very well, so I thought it was now the problem but when I add again this method nothing happened, there are the same mistakes. Is there really a good way to play a sound correctly without problem on android ?

Any help and advice would be really appreciated. If you have any comments or questions, don't hesitate.

Thank you very much for your help.


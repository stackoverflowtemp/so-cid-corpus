Sort of:

<javacode>class X {
X other;
public String toString() {
if(other!=null) return other.toString();
else return super.toString();
}
public void doSomething() {
if(other!=null) { other.doSomething(); return; }
...
}
}
</javacode>

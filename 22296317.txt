here is my code in background service

<javacode>public class BackgroundService extends Service {
private int interval1 = 60; // 60 seconds
private int interval2 = 60; // 60 seconds
//=============================================================================================================================
private Handler mTimer1 = new Handler();
private Runnable mTask1 = new Runnable() {
public void run() {
Looper.prepare();
Log.w("GPS Tracker", "Tracker going to run "+new Date());
LocationManager mlocManager =(LocationManager)getSystemService(Context.LOCATION_SERVICE);
LocationListener mlocListener = new MyLocationListener();
mlocManager.requestLocationUpdates( LocationManager.GPS_PROVIDER, 0, 0, mlocListener);//, Looper.getMainLooper());
mTimer1.postDelayed(this, interval1 * 1000L);
Looper.loop();
}
};


//=============================================================================================================================
private Handler mTimer2 = new Handler();
private Runnable mTask2 = new Runnable() {
public void run() {
if (isOnline() == true) {
Log.w("Method 2", "setUpdateCardAcceptTag");
setUpdateCardAcceptTag();
Log.w("Method 3", "getCardBulkSerialData");
getCardBulkSerialData();
Log.w("Method 12", "updateStockDataFromRemote");
updateStockDataFromRemote();
Log.w("Method 5", "SetRemarksData");
SetRemarksData();
Log.w("Method 6", "SetCardSaleData");
SetCardSaleData();
Log.w("Method 7", "synchMerchants");
synchMerchants();
Log.w("Method 9", "getUpdatedCities");
getUpdatedCities();
Log.w("Method 10", "getNotifications");
getNotifications();
Log.w("Method 11", "getNextSerialDetails");
getNextSerialDetails();
Log.w("Method 12", "getNextSerialDetails");
//synchLocations();
Log.w("Method 13", "synchLocations");

}
mTimer2.postDelayed(this, interval2 * 1000L);
}
};
</javacode>

when i run the application its stopped after few seconds.its says below error message in log console
please help me to sort out this issue

thanks


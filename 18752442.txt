You seem to be misunderstanding. There is always an /sdcard partition, even if it is virtual. But with the 4.2 update, the handling of /sdcard is completely different. Accessing the sdcard partition (in code) no longer points you to the *real* /sdcard, it points you to a virtual "partition" of the sdcard which is specific to the current user.
Keep in mind that the app I'm developing works just fine on Android versions below 4.2, regardless of device.


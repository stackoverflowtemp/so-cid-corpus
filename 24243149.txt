Hello I recently updated may <code>ADT</code> and and now I am developing new app using eclipse. so i want to know where to write <code>setOnclickListner</code> event. I want to open another <code>activity</code> on click button. I tried various combinations but it's gives me error every time. my eclipse Version: 8.1.2.201302132326  please tell me where i can write code??????

after adding  <javacode>setContentView(R.layout.activity_main);</javacode> it gives an Error and application will unfortunately stooped.

I also remove some code and run it but it does not worked.

eg.

<javacode>if (savedInstanceState == null)
{
getFragmentManager().beginTransaction()
.add(R.id.container, new PlaceholderFragment()).commit();

}
</javacode>

and

<javacode>public static class PlaceholderFragment extends Fragment
{

public PlaceholderFragment()
{



}

@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container,
Bundle savedInstanceState)
{
View rootView = inflater.inflate(R.layout.fragment_main, container,
false);

return rootView;
}

}
</javacode>

this code remove from coding but it's not worked.

and in that when i am creating new blank <code>activity</code> then there is fragment activity.xml will created automatically and I want make GUI changes in fragment activity.xml file. If I made changes in activity.xml then it gives error.

Please help me.

Thank you in advance..

here is may code which is automatically generated...

<javacode>public class MainActivity extends Activity
{



@Override
protected void onCreate(Bundle savedInstanceState)
{
super.onCreate(savedInstanceState);
setContentView(R.layout.activity_main);



if (savedInstanceState == null)
{
getFragmentManager().beginTransaction()
.add(R.id.container, new PlaceholderFragment()).commit();

}


}

@Override
public boolean onCreateOptionsMenu(Menu menu)
{

// Inflate the menu; this adds items to the action bar if it is present.
getMenuInflater().inflate(R.menu.main, menu);
return true;


}

@Override
public boolean onOptionsItemSelected(MenuItem item)
{
// Handle action bar item clicks here. The action bar will
// automatically handle clicks on the Home/Up button, so long
// as you specify a parent activity in AndroidManifest.xml.
int id = item.getItemId();
if (id == R.id.action_settings)
{
return true;
}

return super.onOptionsItemSelected(item);
}

/**
* A placeholder fragment containing a simple view.
*/
public static class PlaceholderFragment extends Fragment
{

public PlaceholderFragment()
{



}

@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container,
Bundle savedInstanceState)
{
View rootView = inflater.inflate(R.layout.fragment_main, container,
false);

return rootView;
}

}

}
</javacode>


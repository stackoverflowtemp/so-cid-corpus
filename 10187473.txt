Why would your <code>class</code> keep a list of subclasses? And why do you think having getter/setter for every <code>class</code> member is great for oo? Having getter/setter for every member is the same as having every member <code>public</code> which means no encapsulation at all.
Perhaps, you should start from the other end - implement what your program is supposed to do and only after that beautify it.


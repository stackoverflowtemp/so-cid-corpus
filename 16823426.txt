So I've developed an app, wanted to upload it to the play store, but when it said i had to change the <code>package</code> name different from <code>com.example.app_name</code> I changed it.
But now I'm getting an error:

<error>05-29 22:35:15.599: E/AndroidRuntime(2196): java.lang.IllegalStateException: Could not execute method of the activity
05-29 22:35:15.599: E/AndroidRuntime(2196):     at android.view.View$1.onClick(View.java:3039)
05-29 22:35:15.599: E/AndroidRuntime(2196):     at android.view.View.performClick(View.java:3480)
05-29 22:35:15.599: E/AndroidRuntime(2196):     at android.view.View$PerformClick.run(View.java:13983)
05-29 22:35:15.599: E/AndroidRuntime(2196):     at android.os.Handler.handleCallback(Handler.java:605)
05-29 22:35:15.599: E/AndroidRuntime(2196):     at android.os.Handler.dispatchMessage(Handler.java:92)
05-29 22:35:15.599: E/AndroidRuntime(2196):     at android.os.Looper.loop(Looper.java:137)
05-29 22:35:15.599: E/AndroidRuntime(2196):     at android.app.ActivityThread.main(ActivityThread.java:4340)
05-29 22:35:15.599: E/AndroidRuntime(2196):     at java.lang.reflect.Method.invokeNative(Native Method)
05-29 22:35:15.599: E/AndroidRuntime(2196):     at java.lang.reflect.Method.invoke(Method.java:511)
05-29 22:35:15.599: E/AndroidRuntime(2196):     at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:784)
05-29 22:35:15.599: E/AndroidRuntime(2196):     at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:551)
05-29 22:35:15.599: E/AndroidRuntime(2196):     at dalvik.system.NativeStart.main(Native Method)
05-29 22:35:15.599: E/AndroidRuntime(2196): Caused by: java.lang.reflect.InvocationTargetException
05-29 22:35:15.599: E/AndroidRuntime(2196):     at java.lang.reflect.Method.invokeNative(Native Method)
05-29 22:35:15.599: E/AndroidRuntime(2196):     at java.lang.reflect.Method.invoke(Method.java:511)
05-29 22:35:15.599: E/AndroidRuntime(2196):     at android.view.View$1.onClick(View.java:3034)
05-29 22:35:15.599: E/AndroidRuntime(2196):     ... 11 more
05-29 22:35:15.599: E/AndroidRuntime(2196): Caused by: java.lang.NullPointerException
05-29 22:35:15.599: E/AndroidRuntime(2196):     at com.gip.icomplain.Home.getGPSLocation(Home.java:209)
05-29 22:35:15.599: E/AndroidRuntime(2196):     at com.gip.icomplain.Home.send(Home.java:130)
05-29 22:35:15.599: E/AndroidRuntime(2196):     ... 14 more
</error>

here is my code:

<javacode>@Override
public void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
this.setContentView(R.layout.activity_home);

LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
imv = (ImageView) findViewById(R.id.imvFoto);
onderwerp = (TextView) findViewById(R.id.txtSubject);
commentaar = (TextView) findViewById(R.id.txtComment);
if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
showGPSDisabledAlertToUser();
}
}
public void getGPSLocation() {
if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
showGPSDisabledAlertToUser();
}
LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MINIMUM_TIME_BETWEEN_UPDATES, MINIMUM_DISTANCE_CHANGE_FOR_UPDATES, new MyLocationListener());
currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

if (currentLocation != null) {
longitude = currentLocation.getLongitude();
latitude = currentLocation.getLatitude();
}
EXTRA_TEXT = "http://maps.google.com/maps?q=" + latitude + "," + longitude;
}
}
</javacode>

Please help, this is very important!!!!


I have the following code in Flickr_Dialog_Activity.Java  which shows error .

<javacode>protected void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.flickr_activity);

Button btnFlickr = (Button) findViewById(R.id.btnFlickr);
btnFlickr.setOnClickListener(mFlickrClickListener);

Button btnPick = (Button) findViewById(R.id.btnPick);
btnPick.setOnClickListener(mPickClickListener);

}
</javacode>

I have added the following line after which the problem is solved . The line is

<javacode>import com.carouseldemo.main.R;</javacode>

Here is a note . The Flickr_Dialog_Activity.Java file is located in com.MySocialHub.Flickr project . after that when I try to attempt to run this file I have got the following error .

<error>[2014-03-16 03:00:43 - My Social Hub] Dx
trouble processing "java/security/Provider.class":

Ill-advised or mistaken usage of a core class (java.* or javax.*)
when not building a core library.
</error>
This is often due to inadvertently including a core library file
in your application's project, when using an IDE (such as
Eclipse). If you are sure you're not intentionally defining a
core class, then this is the most likely explanation of what's
going on.

However, you might actually be trying to define a <code>class</code> in a core
namespace, the source of which you may have taken, for example,
from a non-Android virtual machine project. This will most
assuredly not work. At a minimum, it jeopardizes the
compatibility of your app with future versions of the platform.
It is also often of questionable legality.

If you really intend to build a core library -- which is only
appropriate as part of creating a full virtual machine
distribution, as opposed to compiling an application -- then use
the "<code>--core-library</code>" option to suppress this error message.

If you go ahead and use "<code>--core-library</code>" but are in fact
building an application, then be forewarned that your application
will still fail to build or run, at some point. Please be
prepared for angry customers who find, for example, that your
application ceases to function once they upgrade their operating
system. You will be to blame for this problem.

If you are legitimately using some code that happens to be in a
core package, then the easiest safe alternative you have is to
repackage that code. That is, move the classes in question into
your own package namespace. This means that they will never be in
conflict with core system classes. JarJar is a tool that may help
you in this endeavor.


I have not got the reason of getting this error . Can you help me to eradicate this error ?


Your array initialization code before calling <javacode>areasList = mDbHelper.getAllAreas();</javacode> is without effect. You can remove it.
Other than that: did you verify that your DB does *not* contain null values? (You can enforce that with the constraint <code>NOT NULL</code>).


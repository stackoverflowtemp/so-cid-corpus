I am still newbie to android, please could someone help.
I want to use methods from the <code>Net class</code> as follows:

<javacode>package com.test;


import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

public class MyApp extends Activity {
/** Called when the activity is first created. */
private Net wifi;


TextView textStatus;    @Override

public void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);

requestWindowFeature(Window.FEATURE_NO_TITLE);
setContentView(R.layout.main);


wifi=new Net(this);
textStatus = (TextView) findViewById(R.id.text);
textStatus.append("Your online status is ");

if (wifi.isOnline()){

textStatus.append("online "+wifi.getInfo());
}
else{
textStatus.append("offline "+wifi.getInfo());

}
}
}
</javacode>



and my <code>Net class</code>

<javacode>import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.IBinder;



public class Net{
WifiManager wifi;
ConnectivityManager cm ;
NetworkInfo netInfo ;

public Net (Context ctx) {
cm = (ConnectivityManager) ctx.getSystemService(ctx.CONNECTIVITY_SERVICE);
netInfo = cm.getActiveNetworkInfo();
wifi = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
}

public boolean isOnline() {

netInfo = cm.getActiveNetworkInfo();

if (netInfo != null &amp;&amp; netInfo.isConnectedOrConnecting()) {
return true;
}
else{
return false;
}

}
public NetworkInfo[] getName(){



NetworkInfo[] name=cm.getAllNetworkInfo();
return name;

}

public String getInfo(){


// Get WiFi status
WifiInfo info = wifi.getConnectionInfo();
return info.getSSID().toString();
}

}
</javacode>

I believe I should not be extending my <code>Net class</code> with <code>Activity</code>?.... I am getting source not found error when run the app.
Tnx in advance!


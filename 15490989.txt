I have a database handler <code>class</code> that i use to query a database and return a cursor.

This is the method:

<javacode>public Cursor getData() {

String[] columns = new String[] { KEY_ROWID, KEY_NAME, KEY_TEL,
KEY_EMAIL, KEY_COMMENTS };
Cursor c = ourDatabase.query(DATABASE_TABLE, columns, null, null, null,
null, KEY_NAME + " ASC", null);

if (c != null) {

c.moveToFirst();
}

return c;
}
</javacode>
As can be seen i set the pointer to the first element in the cursor ready to be returned. I am currently receiving a '<error>Finalizing a Cursor that has not been deactivated or closed</error>' exception and this is the only <code>cursor</code> I am using where i do not explicitly <code>call .close()</code> on it. My reasons for this is it needs a return type and i cannot close the <code>cursor</code> before returning it as this will set a nullpointer error.

Here is the how the <code>cursor</code> is handled once it is returned:

<javacode>DBHandler search = new DBHandler(this, null, null);

search.open();
Cursor cursor = search.getData();
search.close();
startManagingCursor(cursor);
</javacode>

Can someone point me in the right direction to help close the cursor in my database handler <code>class</code>.


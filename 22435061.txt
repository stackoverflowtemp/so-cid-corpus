I can't figure out what's going wrong here...I've tried writing this more succicinctly, that didn't work. I put in all the extra strings after reading other suggestions with this problem.  Not helping.  No clue what's happening.  Could it be permissions-related?  AFAIK I'm trying to write to internal memory and that doesn't need special permissions?

<javacode>public void outputBitmap(){
String path = Environment.DIRECTORY_PICTURES.toString();
File folder = new File(path + "/Blabla");
String filename = new SimpleDateFormat("yyMMddHHmmss").format(Calendar.getInstance().getTime()) + ".png";
try {
if (!folder.exists()) {
folder.mkdirs();
System.out.println("Making dirs");
}
File myFile = new File(folder.getAbsolutePath(), filename);
myFile.createNewFile();

FileOutputStream out = new FileOutputStream(myFile);
myBitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
out.flush();
out.close();

} catch (Exception e) {
System.out.println(e.getMessage());
e.printStackTrace();
}
}
</javacode>

It goes "Making dirs" every time, the directory is not staying made, or something.  When it gets to <javacode>myFile.createNewFile();</javacode> it gives the error message "<error>open failed: ENOENT (No such file or directory)</error>"

Not sure if it's related, but the information I am trying to output is from:

<javacode>protected void onDraw(Canvas canvas) {
super.onDraw(canvas);

myBitmap = Bitmap.createBitmap(viewWidth, viewHeight, Bitmap.Config.RGB_565);
Canvas pngCanvas = new Canvas(myBitmap);
...</javacode>[some maths and stuff]
<javacode>canvas.drawLine(...);
pngCanvas.drawLine(...);
}
</javacode>

I thought I should be able to use the same canvas for the bitmap, but that caused crashed, so I'm writing the same information to both canvases.  So...I don't know if that's related to the issue or a totally different bad issue or what.

Been searching all kinds of questions that seemed similar, but couldn't find any solutions that worked for me.  I've been trying to solve this for days now.  Anyone know what's going wrong?

Thanks


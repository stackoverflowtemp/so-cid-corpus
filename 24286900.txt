My android app has recently been suspended from the play store because of the play store policy:


Do not send SMS, email, or other messages on behalf of the user without providing the user with the ability to confirm content and intended recipient.


My application is mainly a <code>vas</code> sms application for a telecommunication provider. Now am trying to create a confirmation for all sms send using <code>DialogFrament</code>. What I want is to have a single dialog <code>class</code> and method that will be reuse for all sms sending confirmation. I have look through the forum but can find what am looking for.

What is manage to have was

<javacode>public class sendSMS extends Activity
{

public sendSMS(final String phoneNo, final String sms)
{
DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener()
{

@Override
public void onClick(DialogInterface dialog, int which)
{
switch (which)
{
case DialogInterface.BUTTON_POSITIVE:
//Do your Yes progress
try
{
SmsManager smsManager = SmsManager.getDefault();
smsManager.sendTextMessage(phoneNo, null, sms, null, null);
Toast.makeText(getApplicationContext(),
"Request Sent!",
Toast.LENGTH_LONG).show();
// finish();
}
catch (Exception e)
{
Toast.makeText(getApplicationContext(),
"Request faild, please try again later!",
Toast.LENGTH_LONG).show();
e.printStackTrace();
}
break;

case DialogInterface.BUTTON_NEGATIVE:
//Do your No progress
dialog.cancel();

break;
}
}
};


AlertDialog.Builder ab = new AlertDialog.Builder(this);
ab.setMessage("Are you sure to delete?").setPositiveButton("Yes", dialogClickListener)
.setNegativeButton("No", dialogClickListener).show();

return;
//AlertDialog alertDialog = ab.create();
// alertDialog.show();
}
}
</javacode>

And I am calling it in my <code>activity</code> as:

<javacode>new sendSMS(phoneNo, sms);</javacode>

But whenever I click on the function the app crashes with a <error>NullPointerException</error> error. I need help as I have several sms trigger methods rewriting the confirmation over and over just seems a bit over kill.


For those unsure of how to use PDO (coming from the <code>mysql_</code> functions), I made a very, very simple PDO wrapper that is a single file. It exists to show how easy it is to do all the common things applications need done. Works with PostgreSQL, MySQL, and SQLite.

Basically, read it while you read the manual to see how to put the PDO functions to use in real life to make it simple to store and retrieve values in the format you want.

I want a single column

<code>$count = DB::column('SELECT COUNT(*) FROM `user`);</code>
I want an <code>array(key => value)</code> results (i.e. for making a <code>selectbox</code>)

<code>$pairs = DB::pairs('SELECT `id`, `username` FROM `user`);</code>
I want a single row result

<code>$user = DB::row('SELECT * FROM `user` WHERE `id` = ?', array($user_id));</code>
I want an array of results

<code>$banned_users = DB::fetch('SELECT * FROM `user` WHERE `banned` = ?', array(TRUE));</code>
@yes123, there is no namespacing in my example. It is simply a <code>static class</code>.
I think you're confused about the difference between namespaces and proper inheritance design. If it was named something like <code>Micro_DB</code> (part of a Micro library) then I would need to update it to <code>\Micro\DB</code> in order to be PHP 5.3+ compliant and maintain the correct form of <code>class</code> segregation. However, it is merely a single <code>class</code> without any form of project/library designation. Perhaps you see all <code>static</code> classes as a namespace of sorts, however the <code>class</code> does maintain state and shared variables even though it's not initialized like an object, which means it's not just a namespaced wrapper.
Oh yes, <code>static</code> methods are generally always a sign of bad design. I was just saying that it had nothing to do with namespacing. However, I believe my tiny library is an exception since the point was simply an illustrative library which 1) would never be extended and 2) would never have unit tests (do to the target audience and size of the codebase). My real database/ORM libraries are all namespace classes which are to be used as real and extendable objects as they should be.


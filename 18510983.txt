<javacode>Date dat = new Date();
Calendar cal_alarm = Calendar.getInstance();
Calendar cal_now = Calendar.getInstance();
cal_alarm.setTime(dat);
cal_alarm.set(Calendar.HOUR_OF_DAY, hrs);// set the alarm time
cal_alarm.set(Calendar.MINUTE, min);
cal_alarm.set(Calendar.SECOND, 0);
cal_alarm.set(Calendar.MILLISECOND, 0);
if (cal_alarm.before(cal_now)) {// if its in the past increment
cal_alarm.add(Calendar.DATE, 1);
}

Intent intent = new Intent(ctx, AlarmReceiver.class);
// intent.putExtra("Reminder to Take Photo", "Pixitch!");
PendingIntent sender = PendingIntent.getBroadcast(ctx, 0010000,
intent, 0);
// Get the AlarmManager service
long tmemills = cal_alarm.getTimeInMillis()
- cal_now.getTimeInMillis();
AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
am.setRepeating(AlarmManager.RTC_WAKEUP, tmemills,
AlarmManager.INTERVAL_DAY, sender);
</javacode>

Alarm Receiver Class

<javacode>public class AlarmReceiver extends BroadcastReceiver {

private static final int MY_NOTIFICATION_ID = 1;
private NotificationManager notificationManager;
private Notification myNotification;

// Context ctx = this;

@SuppressWarnings("deprecation")
@Override
public void onReceive(Context context, Intent intent) {
try {
NotificationManager mNM;
mNM = (NotificationManager) context
.getSystemService(Context.NOTIFICATION_SERVICE);
Notification notification = new Notification(
R.drawable.ic_launcher, "Pixitch Notification !",
System.currentTimeMillis());
PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
new Intent(context, AlarmManage.class), 0);
// Set the info for the views that show in the notification panel.
notification.setLatestEventInfo(context, "Pixitch Notification!",
"Reminder For TakePhoto", contentIntent);
mNM.notify(0, notification);
} catch (Exception e) {
Toast.makeText(
context,
"There was an error somewhere, but we still received an alarm",
Toast.LENGTH_SHORT).show();
e.printStackTrace();

}
}
}
</javacode>


the value of <code>tmemills</code> is 278,088

the <code>tmemills</code> is around 4.5 minutes but

the alarm Manager is Running immediately

I am not able to find where the problem is because I am beginner for Android. please help me


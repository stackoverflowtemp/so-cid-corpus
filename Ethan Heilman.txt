Update:

Times have changed, we have a SHA3 winner. I would recommend using keccak (aka SHA3) winner of the SHA3 contest.

Original Answer:

In order of weakest to strongest I would say:

RIPEMD BROKEN, Should never be used as can be seen in this pdf
MD-5 BROKEN, Should never be used, can be broken in 2 minutes with a laptop
SHA-1 BROKEN, Should never be used, is broken in principal, attacks are getting better by the week
SHA-2 WEAK, Will probably be broken in the next few years. A few weaknesses have been found. Note that generally the higher key size, the harder the hash function is to break. While key size = strength is not always true, it is mostly true. So SHA-256 is probably weaker than SHA-512.
Skein NO KNOWN WEAKNESSES, is a candidate for SHA-3. It is fairly new and thus untested. It has been implemented in a bunch of languages.
MD6 NO KNOWN WEAKNESSES, is another a candidate for SHA-3. Probably stronger than Skien, but slower on single core machines. Like Skien it is untested. Some security minded developers are using it, in mission critical roles.
Personally I'd use MD6, because one can never been too paranoid. If speed is a real concern I'd look at Skein, or SHA-256.
I agree with your sentiments, but I think the community is in a strange position. All the hash functions in use are dangerously close to being broken (maybe, maybe, not SHA2 256-512) and yet we have to wait to 2012 to choose a replacement. choose your poison: weak/broken or untested (most NIST candidates have not been public for more than 6 months)? Tough choice.
@Cocowalla I don't think we are likely to see full support any contestants, until one of them is announced the winner.
hash functions typically don't use keys


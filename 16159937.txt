Not sure whats wrong here. The crash happens if there is nothing in the <code>edittext</code> spots. It works if they are all filled out. So my problem is the if statement check. It is going into the <code>IF</code> statement even if the <code>edittext</code> are empty. It is supposed to skip to the <code>else</code> and log that not all fields are completed. I have checked EVERYTHING (that I know as a noob obviously). Thanks for the help guys!

http://codeviewer.org/view/code:323c


The problem is here (I think) it is jumping into the loop even tho it is not supposed to:

<javacode>
//Calculate Risk Score Button
Button calcButton = (Button) findViewById(R.id.calcButton);

calcButton.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {

// Create and set values
int smoker = 0;
int medicated = 0;
char gender = 'm';
String selectedAge = (String) ageSpin.getSelectedItem();
Log.i("TEST", "Before if");

if (selectedAge != null
&amp;&amp; cholEdit.getText().toString() != null
&amp;&amp; hdlEdit.getText().toString() != null
&amp;&amp; sbpEdit.getText().toString() != null
&amp;&amp; genderGroup.getCheckedRadioButtonId() != -1
&amp;&amp; smokerGroup.getCheckedRadioButtonId() != -1
&amp;&amp; medsGroup.getCheckedRadioButtonId() != -1) {
Log.i("TEST", "After if");

// store values
int age = Integer.parseInt(selectedAge.toString());
int chol = Integer.parseInt(cholEdit.getText().toString());
int hdl = Integer.parseInt(hdlEdit.getText().toString());
int sbp = Integer.parseInt(sbpEdit.getText().toString());
int genderId = genderGroup.getCheckedRadioButtonId();
int smokeId = smokerGroup.getCheckedRadioButtonId();
int medsId = medsGroup.getCheckedRadioButtonId();

// Set gender, smoker and if medicated values
if (genderId == R.id.maleR)
gender = 'm';
else if (genderId == R.id.femaleR)
gender = 'f';

if (smokeId == R.id.yesR)
smoker = 1;
else if (smokeId == R.id.noR)
smoker = 0;

if (medsId == R.id.yesR2)
medicated = 1;
else if (medsId == R.id.yesR2)
medicated = 0;

// Calculate Answer and Print
String answer = calc.calculateRiskScore(age, gender, chol,
smoker, hdl, sbp, medicated);


SharedPreferences settings = getSharedPreferences(
PREF_FILE, 0);
SharedPreferences.Editor editor = settings.edit();

editor.putInt("age", age);
editor.putInt("chol", chol);
editor.putInt("hdl", hdl);
editor.putInt("sbp", sbp);
editor.putInt("gender", genderId);
editor.putInt("smoker", smoker);
editor.putInt("med", medicated);
editor.putString("risk", answer);

editor.commit();

displayRisk(answer);
} else {
Toast toast = Toast.makeText(getApplicationContext(),
"Not all fields are completed", Toast.LENGTH_SHORT);
toast.show();
}
}
});
}
</javacode>

here is the logcat error I get what the error is saying but it should not be in the <code>if</code> statement to make it:

<error>04-22 21:26:43.920: I/TEST(22544): Before if
04-22 21:26:43.920: I/TEST(22544): After if
04-22 21:26:43.920: W/dalvikvm(22544): threadid=1: thread exiting with uncaught exception (group=0x41634438)
04-22 21:26:43.930: E/AndroidRuntime(22544): FATAL EXCEPTION: main
04-22 21:26:43.930: E/AndroidRuntime(22544): java.lang.NumberFormatException: Invalid int: ""
04-22 21:26:43.930: E/AndroidRuntime(22544):    at java.lang.Integer.invalidInt(Integer.java:138)
04-22 21:26:43.930: E/AndroidRuntime(22544):    at java.lang.Integer.parseInt(Integer.java:359)
04-22 21:26:43.930: E/AndroidRuntime(22544):    at java.lang.Integer.parseInt(Integer.java:332)
04-22 21:26:43.930: E/AndroidRuntime(22544):    at edu.auburn.eng.csse.comp3710.group04.CardiovascularActivity$1.onClick(CardiovascularActivity.java:83)
04-22 21:26:43.930: E/AndroidRuntime(22544):    at android.view.View.performClick(View.java:4198)
04-22 21:26:43.930: E/AndroidRuntime(22544):    at android.view.View$PerformClick.run(View.java:17164)
04-22 21:26:43.930: E/AndroidRuntime(22544):    at android.os.Handler.handleCallback(Handler.java:615)
04-22 21:26:43.930: E/AndroidRuntime(22544):    at android.os.Handler.dispatchMessage(Handler.java:92)
04-22 21:26:43.930: E/AndroidRuntime(22544):    at android.os.Looper.loop(Looper.java:137)
04-22 21:26:43.930: E/AndroidRuntime(22544):    at android.app.ActivityThread.main(ActivityThread.java:4918)
04-22 21:26:43.930: E/AndroidRuntime(22544):    at java.lang.reflect.Method.invokeNative(Native Method)
04-22 21:26:43.930: E/AndroidRuntime(22544):    at java.lang.reflect.Method.invoke(Method.java:511)
04-22 21:26:43.930: E/AndroidRuntime(22544):    at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:1004)
04-22 21:26:43.930: E/AndroidRuntime(22544):    at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:771)
04-22 21:26:43.930: E/AndroidRuntime(22544):    at dalvik.system.NativeStart.main(Native Method)
</error>

